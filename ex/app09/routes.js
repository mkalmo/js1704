(function () {
    'use strict';

    angular.module('app').config(Conf);

    function Conf($routeProvider) {

        $routeProvider.when('/list', {
            templateUrl : 'app09/list.html',
            controller : 'ListCtrl',
            controllerAs : 'vm'
        }).when('/new', {
            templateUrl : 'app09/add.html',
            controller : 'AddCtrl',
            controllerAs : 'vm'
        }).otherwise('/list');
    }

})();