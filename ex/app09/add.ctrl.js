(function () {
    'use strict';

    angular.module('app').controller('AddCtrl', Ctrl);

    function Ctrl($http, $location) {

        var vm = this;
        vm.title = '';
        vm.text = '';
        vm.addNew = addNew;

        function addNew() {
            var newPost = {
                title : vm.title,
                text : vm.text
            };

            $http.post('api/posts', newPost).then(function () {
                $location.path('/list');
            });
        }

    }

})();
