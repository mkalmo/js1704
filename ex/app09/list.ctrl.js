(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('ListCtrl', Ctrl);

    function Ctrl($http) {

        var vm = this;
        vm.posts = [];
        vm.deletePost = deletePost;

        init();

        function init() {
            $http.get('/posts').then(function (result) {
                vm.posts = result.data;
            });
        }

        function deletePost(postId) {
            $http.delete('/posts/' + postId).then(init);
        }


    }

})();
